oStorybook

Please check http://ostorybook.eu for more
---
Please install or update Java (minimal version 8).
---
Which file to use?

Linux:
- Debian users : oStorybook-X.ZZ.deb
- RPM based users : ostorybook-X.ZZ.rpm
- All others Linux users : oStorybook-X.ZZ.zip
(unzip this file into your oStorybook folder)

Mac OS X : oStorybook-X.ZZ-macOSx.tar.gz
(extract this content into your oStrybook folder)

Windows users : oStorybook-X.ZZ.exe

where X is the version major, and ZZ is the version minor

---
Please install or update Java (minimal version 8).
---
