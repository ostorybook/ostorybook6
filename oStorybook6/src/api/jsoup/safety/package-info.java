/**
 Contains the jsoup HTML cleaner, and whitelist definitions.
 */
package api.jsoup.safety;
