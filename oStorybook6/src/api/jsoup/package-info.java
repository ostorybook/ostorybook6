/**
 Contains the main {@link api.jsoup.Jsoup} class, which provides convenient static access to the jsoup functionality. 
 */
package api.jsoup;