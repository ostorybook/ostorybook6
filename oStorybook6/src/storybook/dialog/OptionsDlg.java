/*
 * Copyright (C) 2017 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.dialog;

import storybook.ui.panel.tree.TreeOpt;
import storybook.ui.panel.timeline.TimelineOpt;
import storybook.ui.panel.manage.ManageOpt;
import storybook.ui.panel.chrono.ChronoOpt;
import storybook.db.book.OptBook;
import java.awt.event.ActionEvent;
import javax.swing.JTabbedPane;
import api.mig.swing.MigLayout;
import resources.icons.IconUtil;
import i18n.I18N;
import storybook.ui.MIG;
import storybook.ui.MainFrame;
import storybook.ui.SbView;

/**
 *
 * @author favdb
 */
public class OptionsDlg extends AbsDialog {

	private final String sbView;
	private OptBook bookOpt;
	private ChronoOpt chronoOpt;
	private ManageOpt manageOpt;
	private TimelineOpt timelineOpt;
	private TreeOpt treeOpt;

	public OptionsDlg(MainFrame m) {
		super(m);
		sbView = null;
		init();
		initUi();
	}

	public OptionsDlg(MainFrame m, String v) {
		super(m);
		sbView = v;
		init();
		initUi();
	}

	public static void show(MainFrame m, String v) {
		OptionsDlg dlg = new OptionsDlg(m, v);
		dlg.setVisible(true);
	}

	@Override
	public void init() {
		// empty
	}

	@Override
	public void initUi() {
		bookOpt = new OptBook(mainFrame);
		chronoOpt = new ChronoOpt(mainFrame);
		manageOpt = new ManageOpt(mainFrame);
		timelineOpt = new TimelineOpt(mainFrame);
		treeOpt = new TreeOpt(mainFrame);
		//layout
		setLayout(new MigLayout(MIG.get(MIG.FILL, MIG.WRAP1)));
		setTitle(I18N.getMsg("options"));
		setIconImage(IconUtil.getIconImage("icon"));
		if (sbView == null) {
			JTabbedPane tabbed = new JTabbedPane();
			tabbed.add(I18N.getMsg("view.book"), bookOpt);
			tabbed.add(I18N.getMsg("view.chrono"), chronoOpt);
			tabbed.add(I18N.getMsg("view.manage"), manageOpt);
			tabbed.add(I18N.getMsg("view.tree"), treeOpt);
			add(tabbed);
		} else {
			switch (SbView.getVIEW(sbView)) {
				case BOOK:
					setTitle(I18N.getMsg("view.book"));
					add(bookOpt);
					break;
				case CHRONO:
					setTitle(I18N.getMsg("view.chrono"));
					add(chronoOpt);
					break;
				case MANAGE:
					setTitle(I18N.getMsg("view.manage"));
					add(manageOpt);
					break;
				case TIMELINE:
					setTitle(I18N.getMsg("view.timeline"));
					add(timelineOpt);
					break;
				case TREE:
					setTitle(I18N.getMsg("view.tree"));
					add(treeOpt);
					break;
			}
		}
		add(getCloseButton(), MIG.get(MIG.SPAN, MIG.SG, MIG.RIGHT));
		pack();
		setLocationRelativeTo(mainFrame);
		setModal(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// empty
	}

}
