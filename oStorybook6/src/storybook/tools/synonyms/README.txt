Traitement Synonymes et Antonymes
Note importante: les variantes de langues ne sont pas prises en charge.
Exemple: pt_PT et pt_BR (brésilien) renvois sur pt.

1. Recherche
a) Se baser sur le SEARCH_00 pour créer la procédure spécifique pour la langue.
b) ajouter le traitement de la recherche dans SEARCH.java

2. Dictionnaires (ajouter les infos au fur et à mesure)
ca (Catalan):
source: projet LibreOffice, pas d'antonymes
processus: ExtractDico
recherche en ligne sur: Institut d'Estudis Catalans

en (Anglais):
source: projet LibreOffice, pas d'antonymes
processus: ExtractDico
recherche en ligne sur: Merriam-Webster's dictionary

fr (Français):
source générale: DES du CRISCO, lemmes depuis Dicollecte
processus: ExtractDico des deux sources, tri et fusion avec élimination des doublons, 
recherche en ligne sur: 

hu (Hongrois):
source générale: LibreOffice, pas d'antonymes
processus: ExtractDico 
recherche en ligne sur: Midnet Digital Kft. https://topszotar.hu/ 

pt (Portugais):
source générale: LibreOffice, pas d'antonymes
processus: ExtractDico 
recherche en ligne sur: www.dicionario-sinonimo.com (propriétaire indéterminé)

