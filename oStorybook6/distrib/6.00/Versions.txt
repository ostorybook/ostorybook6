oStorybook
==========

Versions :
6.00: (21/04/2024)
- nouvelle structure sans utiliser le SGBD H2Database ni Hibernate
5.61: (31/12/2023)
- ajout de racourcis clavier pour SHEF (style paragraphe, listes)
- ajout de l'export TeX/LaTeX
- correction anomalie Infos rapides

5.60: (01/09/2023)
- ajout de la boîte à idées
- ajout du show/hide dans Shef
- amélioration personnalisation couverture EPUB
- réduction de la barre d'outils de l'éditeur de texte
- ajout du traitement du trait d'union et de l'apostrophe dans JOrtho
- ajout de détails dans table des parties
- ajout activation Orthographe
- traitement du signe @ dans un titre (partie, chapitre, scène)
- ajout de la dédicace
- correction de la copie d'éléments

5.59:(21/04/2023)
- integration de la personnalisation/traduction de l'Assistant
- ajout du Ctrl+Maj+V
- ajout du Zoom à l'editeur
- ajout de la recherche de synonymes et antonymes
- ajout de la fonction relecture (Comment/Review)

5.05.08: (31/12/2022)
- amélioration de la recherche d'un mot
- ajout de raccourcis clavier dans SHEF (espace insécable, cadratin, BR)
- ajout de l'import EPUB comme nouveau projet
- ajout de l'assistant pour les Fils et les Parties
- refonte de l'assistant (format XML)

5.05.07: (04/09/2022)
- possibilité de personnaliser la couleur des intensités
- amélioration de la gestion de l'Assistant
- Memo modifiable directement dans la vue des mémos
- amélioration de l'interface graphique (meilleure flexibilité)
- amélioration de l'interface Episode
- possibilité de modifier directement la taille objectif dans le tableau des 
parties et des chapitres
- ajout de la visualisation d'objectif dans la barre d'état et le tableau des 
chapitres
- correction du bug dans l'outil de traduction (I18N)

5.05.06: (21/04/2022)
- ajout Episodes
- ajout du Storymap
- correction du status des Ideas
- correction bug vue Memoria
- ajout de la possibilité de modifier la taille des icones
- modification nouveau projet pour ajout objectif
- ajout de la possibilité de créer plusieurs parties, ou scènes, en même temps
- ajout multi modification pour les scènes
- correction vue Arbre
- finalisation de l'export DOCX (avec limitation)
- ajout de l'intensité dans Scene avec visualisation dans Chapter
- ajout de la possibilité d'utiliser une ressource locale (dossier ou fichier) dans SHEF en tant qu'hyperlien

5.05.05: (01/01/2022)
- corrections diverses
- suppression API swingX
- modification de l'aspect des tables pour les listes (accolades si plusieurs elements) 
- ajout de la fonction de détection automatique des liens Personnages, Lieux ou Objets
- ajout des fonctions ODT et DOCX, et amélioration Import/Export
- suppression API JOpenDocument

5.05.04: (01/09/2021)
- corrections mineures (EPUB+Editeur)
- assistant fonctionnel
- amelioration graphique pour les rapports (couleurs sombres)
- exportation des fiches (vue info), avec modification du dialogue
- exportation de la vue info en Html
- #110 implemented (exportation des tableaux "as is" (colonnes visibles seulement))
- ajout du choix de la couleur pour le TimeEvent
- ajout de la vue Timeline
- ajout du paramétrage pour troncature dans la vue Arbre
- activation des liens Web (vues Lire, Info, Memos et editeur)
- remise en ordre de la date de la scène (fixe, relative)
- ajout de la durée pour une scène
- ajout de la durée pour un événement
- réactivation des raccourcis undo/redo avec ajout des boutons
- ajout du stabylo
- possibilité de basculer du mode HTML au Markdown, et retour
- possibilité d'activer/désactiver l'option scénario
- extension du mode scénario à toutes les fonctions (table des scène, editeur de scène)
- #119 fixed (largeur des colonnes dans la vue "Gérer les chapitres et les scènes")

5.05.03: (25/12/2020)
- activation des racourcis clavier dans l'éditeur
- possibilité de choisir son L&F
- ajout du L&F sombre
- resolution de bugs dans les vues Memoria et Livre
- harmonisation de l'éditeur
- ajout de la possibilité de choisir le mode de présentation de l'éditeur pour
  description et notes
- ajout du bouton deploy dans l'éditeur
- remise en place de l'option de suppression d'un élément dans le menu
  contextuel de la vue Arbre
- ajout de l'option de couleur dans les preferences
- révision de la migration de la BDD
- ajout de la memorisation de la configuration pour les vues Arbre, Chrono, Memo 
- ajout du Catalan

5.05.02: (01/09/2020)
- affinage export epub
- ajout Endnotes
- correction bug Memo

5.05.01: (01/08/2020)
- ajout de l'export epub
- ajout toolbar pour les tableaux (filtrage)
- correction bug sur nom de fichier (apostrophe)

5.05.00: (01/07/2020)
- correction anomalie JOrtho
- implémentation du Scénario et du Markdown
- réécriture d'une large partie du code pour faciliter la maintenance/évolution
- réécriture de l'ensemble de l'éditeur
- modification de la nature du fichier enregistré
- modification de la langue proposée lors du premier lancement
- modification de la gestion du fichier messages externe
- modification de la traduction du I18N (prise en compte du codage UTF8)
- amélioration du filtrage pour le tableau des scènes
- #97 tri des éléments par catégorie

5.04.04d: (07/01/2020) corrections mineures
5.04.04c:
5.04.04b:
5.04.04a:
5.04.04: (13/02/2019)
- correctif d'urgence exportation liste Idea en HTML
5.04.03: (02/11/2018)
- correctif d'urgence exportation HTML
5.04.02: (18/06/2018)
5.04.01: (08/05/2018)

5.04.00: (22/04/2018)
- ajout d'options supplémentaires pour l'exportation en HTML
- finalisation de l'assistant (personnage, lieu, objet)

5.03.00: (31/01/2018)
- ajout du barré, indice, indexe dans l'éditeur
- pour chaque composante ajout d'un getImageIcon
- dans l'éditeur de scène, si un objectif a été assigné dans le chapitre, une
  barre de progression apparaît
- remplacement du module d'insertion d'un caractère Unicode, avec extension à
  tous les blocs Unicode
- ajout de l'insertion du tiret cadratin (et autres caractères spéciaux) dans
  l'éditeur HTML
- ajout de l'assistant
- ajout du mécanisme de sauvegarde/restauration
- résolution du bug Typist
- ajout d'un mécanisme de sauvegarde/restauration lorsqu'on travaille longuement
  sur une scène

5.01.00: (01/09/2017)
- ajout du mode Dactylo
- le bouton des caractères spéciaux est maintenant dans les outils de l'éditeur
- modification editeur Partie (notes sur l'onglet Commun)
- ajout du bouton éditer dans la liste des composantes de l'éditeur
- ajout du nom du fichier externe dans la vue Info et d'un bouton de lancement
- ajout du copier Fils (Strands)
- décision de passer à la 5.01.00 en raison de la modification de l'UI
- réécriture du dialogue de copie des éléments
- accès à l'éditeur de scène sur click droit dans en-tête de colonne de la vue
  "Personnages par scène"
- centralisation des options des vues
- vue de l'occupation mémoire optionnelle dans la barre de status
- ajout de la possibilité de forcer une fin de ligne dans le texte
- ajout de l'option changement de page pour l'export HTML simple fichier
- ajout du filtre "Narrateur" dans la table des Scènes
- ajout de la possibilité de renuméroter les scènes
- ajout des raccourcis clavier dans l'editeur
- ajout dans les préférences de la police pour l'editeur
- intégration des source org.apache.commons.lang3, ...collections

5.00.03: (21/04/2017)
- résolution de l'ordre dans les ComboBox
- simplification des messages pour l'I18N
- révision de la vue "Infos rapides"
- correction du bug sur le nombre de scènes
- ajout de l'option "position" dans la vue "Mémos"
- ajout de l'option "cacher les cènes non affectées" dans la vue "Gérer les 
chapitres et les scènes"
- ajout des fils secondaires dans la table des scènes
- ajout de l'option de gestion en multi écrans
- ajout de la possibilité d'accès aux informations si l'éditeur est ouvert
- création du module d'importation, avec révision du module d'exportation
- réécriture de l'interface utilisateur

5.00.02: (15/01/2017)
- ajout d'un module pour l'aide à l'internationalisation
- correction de l'anomalie du téléchargement des dictionnaires
- résolution du bug #42 (ajout direct personnage, lieu, objet)
- correction du calcul du nombre de mots
- ajout d'un message d'avertissement si l'export existe déjà
- ajout d'un message de fin pour toutes les exportations
- mémorisation de la position et de la taille de l'éditeur (par type d'objet)
- ajout des Memos dans la vue Arbre
- remplacement de l'editeur LibreOffice par un editeur banalisé
- réintroduction de la géolocalisation d'un lieu à partir de l'adresse
- personnalisation de la barre d'outils
- ajout du nombre de scènes dans les chapitres pour la table et l'éditeur et
l'info rapide
- ajout d'une navigation "image" lors de l'export HTML multifichiers
- correction du bug de la vue Lecture
- modification de la présentation de la table des memos
- correction anomalie premier lancement
- réactivation de la sortie rapide de l'éditeur

5.00.01: (01/12/2016)
- amélioration du updater et de la gestion des préférences
- migration des dialogues en NetBeans natif
- intégration des sources infonode et miglayout et suppression des libraries
- changement de gestion des Preferences (remplacé par un ini pour les 
préférences et par des fichiers layout pour les dispositions personnalisées)
- ajout de la possibilité de personnaliser l'icone du genre lors de la création
(donc si ce n'est pas homme ou femme), si l'icone de genre n'est pas
personnalisée c'est l'icone par défaut de personnage qui est affichée/utilisée
- remplacement de tous les messages d'exception par un rendu plus explicite
- extension de la fonction Export en ajoutant le format DocBook et un format
XML Storybook database
- ajout de la fonction export XML pour chacune des tables
- internationalisation de l'interface infonode de gestion des fenêtres
- double vue des attributs (table et liste)
- amélioration du rendu graphique dans la vue lecture et dans l'export HTML
en fonction du contenu du titre d'une scène
- ajout de l'export "didascalie" pour la scène
- réintégration de la vérification orthographique
- restructuration générale des menus
- ajout d'une fonction de recherche et résultats de la recherche cliquable
- petite révision sur le rapport "apparition des personnages par date" 
- ajout du rapport "occurrence des objets"
- validation du CSS personnalisé
- dossier dicts dans le dossier des préférences
- lors de la sélection d'une scène, ou d'un chapitre, pour visualisation dans
vue "gestion", la scène, ou le chapitre, clignote environ 3 secondes en rouge
- ajout dans l'éditeur du nombre de mots après le nombre de caractères

4.10.04: favdb et Jean Rebillat (01/09/2016)
- ajout du drag-n-drop pour les scènes et les chapitres.
- suppression de trace du langagetool, en particulier dans les préférences
générales
- suppression d'un bug si la sortie se fait par l'icône de sortie de la fenêtre
- résolution du bug sur rapport graphique
- suppression de l'usage de la bibliothèque JFree, réécriture du code des
rapports graphiques
- dans toutes les vues, si l'un objet a des notes elles sont signalées par 
une astérisque
- résolution du bug pour les fonctions de changement de nom d'une catégorie
(ville, pays, item, tag)
- ajout de controles supplémentaires pour l'export
- préparation de la migration de tous les dialogues
- migration du dialogue About

4.10.03 : favdb
- intégration du code source de JDateChooser
- ajout d'une vue 'strand' dans Memoria

4.10.2 : favdb et Jean Rébillat 13/12/2015
- suppression du bug Enregistrer Sous et Renommer

4.10.1 : favdb et Jean Rébillat 01/12/2015
- accès aux chapitres depuis la vue "Gérer les chapitres et les scènes"
- mémorisation de la disposition des tableaux
- alignement de la taille des champs notes et description des objets et
  étiquettes sur la même taille que pour les autres éléments (32768)
- correction du bug concernant le format de date à saisir, avec correction
au niveau des préférences et des tableaux
- obligation d'utiliser le OK ou Annuler pour sortir du dialogue editeur
- ajout de la creation d'une idea depuis n'importe quel editeur
- possibilité de changer l'ordre des chapitres
- ajout de la possibilité de sélectionner plusieurs personnages/lieux/objets
dans les relations
- ajout de la possibilité de tester un fichier de traduction
- ajout des Relations dans la vue Memoria
- ajout de l'export SQL

4.10.0 : favdb et Jean Rébillat 01/09/2015
- lien vers fichier ODT modifiable
- tri alphabétique des fonctions de recherche des personnages et lieux
- suppression de la dépendance dans les paquetages deb et rpm
- ajout du Hongrois comme langue de l'interface utilisateur
- nouvelle écriture de la fonction Memoria
- nouvelle fonction de plannification pour l'auteur
- choix d'un modèle simple sans macro pour LibreOffice
- sélection d'un modèle personnel pour LibreOffice
- suppression de la lib languagetool et de toutes les utilisations
- reconfiguration GIT (sans lib, dicts.all)
- refondation du code source

4.9.17 : favdb 21/04/2015
- ajout de la mise en relation entre des personnages
- ajout de l'export au format XML DocBook
- correction de la barre inférieur de statistiques
- ajout du filtre sur les fils dans le tableau des scènes (suggestion 11b)
- correction du bug concernant l'export de la liste des scènes
- ajout de notes à l'élément partie
- ajout d'une fonction d'insertion de caractères spéciaux
- suppression de traces de l'ancienne version "pro"

4.9.16 : favdb 01/12/2014
- mise à jour du panneau d'information sur simple clique de sélection dans l'un
des tableaux
- réintégration de la vue des Attributs au même rang que les autres
- correction du bug concernant la réplication de scène en cas d'ajout dynamique
personnage, lieu ou fil
- modification de l'icone, splash, image
- ajustement de l'I18N pour les menus (mnémoniques)
- redéveloppement du packaging Debian
- ajout du packaging RPM
- internationalisation du "à propos..."
- ajout des statistiques sur la barre d'état
- bug : la vue des idées était inactive
- modification du log4j pour remonter au niveau ERROR
- lieux triés par pays puis ville puis nom (dans l'arbre, Memoria, etc...)

4.9.15 : favdb 01/09/2014
- résolution du bug de l'éditeur par utilisation en mode dialogue
- transformation du menu et de la barre d'outils au format Netbeans en
  vue de la suppression du action.xml
- ajout du téléchargement des outils orthographiques disponibles
- ajout de la fonction d'appel à LibreOffice/OpenOffice.org
- ajout de la possibilité de créer de nouveaux Fils, Personnages
  ou Lieux depuis l'éditeur de scène sans quitter la scène elle-même
- ajout du panneau de visualisation des Attributs
- mise en place des distributions Linux, Debian, MacOSX (à tester)

4.9.14 : favdb 21/04/2014
- mise à jour des sources du site Web (source séparé)
- suppression de toutes les références à JasperReport
- export livre avec mode multi-fichiers HTML

4.9.13 : favdb 09/03/2014
- ré-écriture du module d'exportation (suppression future de JasperReport),
  avec actualisation de la library iText à sa dernière version (5.5.0)
- amorce du module d'impression
- remplacement du dialogue de confirmation d'une suppression par un modèle
  Netbeans (un seul dialogue au lieu du ConfirmDelete et ConfirmMultiDelete)
- mise en place des éditeurs dédiés, sous Netbeans

4.9.12 : favdb 31/01/2014
- nouvel empaquetage pour distributions Linux avec Makeself
- restauration des fonctions export (avec séparation du code entre
  TXT et HTML), reintroduction des JasperReports
- restructuration du HtmlUtil pour uniformisation de l'export HTML
- préparation en vue de la modification du dialogue pour l'export

4.9.11  : favdb, 19/01/2014
- essai pour forcer le panneau editor pour le cas où il apparaît vide
- correction du action.xml (internationalisation du menu Help)
- correction du dialogue d'options (fenêtre trop petite),
  cadrage à gauche et wrap de chaque élément
- restructuration du code (partie interface utilisateur)
- introduction de la nouvelle interface utilisateur
  (non fonctionnelle les actions ne sont pas opérationnelles)
- ajout du paramètre d'exécution "--newUI" pour assurer la
  compatibilité 4.x.x et 5.x.x et permettre le test de la nouvelle interface

4.9.10  : favdb, 16/01/2014
- petites corrections du code
- ajout des fonctions trace et logErr centralisées
- quelques traduction pour le fançais
- adaptation de la détection du changement de version
- internationalisation du About

4.9.9   : favdb ??/10/2013

4.0.9   : Martin Mustun, 13/05/2013
3.1.12  : Martin Mustun
3.0.0   : Martin Mustun, 13/09/2011
2.1.17  : Martin Mustun, 19/08/2011
2.1.15  : Martin Mustun, 20/12/2010
2.1.12  : Martin Mustun, 02/02/2010
2.1.11  : Martin Mustun,
2.1.6   : Martin Mustun, 30/10/2008
2.0.10  : Martin Mustun, 11/01/2008
